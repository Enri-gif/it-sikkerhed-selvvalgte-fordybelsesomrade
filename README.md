# Indlejret system sikkerhed

# Indhold

Der er ikke særlig meget sikkerhed i indlejret systmer og det kan resultere i sårbarheder som kan udnyttes.  
Hvordan kan indlejret systemer sikres?  

# Læringsmål

Den studerende opsætter selv læringsmål (skabelon herunder) ud fra det valgte emne og indenfor rammerne i uddannelsens mål for læringsudbytte fra den nationale studieordning kapitel 1, se: [https://esdhweb.ucl.dk/D22-1980440.pdf](https://esdhweb.ucl.dk/D22-1980440.pdf)  
Det er ikke alle læringsmål der skal opfyldes, vælg dem der passer til det valgte fordybelsesområde.

Læringsmål skal ordnes efter viden, færdigheder og kompetencer

**Viden**

Den studerende har viden om...

- Anvendelse af indlejret systemer
- Managed og unmanaged kode
- Toolchains
- Buffer og buffer overflow
- Firmware
- Crosscompilers


**Færdigheder**

Den studerende kan ...

- Programmere med unmanaged code
- Bruge gdb til at analysere en binary
- Bruge kryptografiske signatur til at verificere filer
- Udføre en OS command injection
- Udføre en buffer overflow angreb

**Kompetencer**

Den studerende kan ...

- Håndtere sårbar funktioner i unmanaged code
- Håndtere gdb til at analysere en binary

# Milepæls plan

Her angives deadlines (datoer) for hvilke milepæle der arbejdes efter i projektet
Der skal angives en milepæl pr. uge fra og med uge 36 til og med uge 46

| Deadline   | Milepæl                                                                                |
| :--------- | :--------------------------------------------------------------------------------------|
| 2023-09-05 | Projekt beskrevet og planlagt                                                          |
| 2023-09-12 | Blink projektet                                                                        |
| 2023-09-19 | Forståelse af Toolchains, toolchain hardening og crosscompiler                         |
| 2023-09-26 | Binary analyse af Blink projektet                                                      |
| 2023-10-03 | Forståelse af buffer og buffer overflow angreb                                         |
| 2023-10-10 | Gøre mig bekendte med gdb                                                              |
| 2023-10-17 | Binary analyse af en buffer overflow shellcode insertion angreb                        |
| 2023-10-24 | Binary analyse af en buffer overflow angreb                                            |
| 2023-10-31 | Binary analyse af en buffer overflow angreb og analyse af c kode med flawfinder        |
| 2023-11-07 | OS command injection og opdatering af firmware med kryptografiske signaturer           |
| 2023-11-14 | Eksamensaflevering færdig                                                              |

# Link til gitlab page

https://enri-gif.gitlab.io/page/selvvalgteemne/
#include <stdio.h>
#include <string.h>

void Granted()
{
    printf("access granted");
}

int main(int argc, char* argv[])
{
    char buff[256];
    strcpy(buff, argv[1]);
    char password[] = "password";
    if(strcmp(buff, password) == 0)
        Granted();
    else
        printf("access denied");

    return 0;
}